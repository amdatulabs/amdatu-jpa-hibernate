/**
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.jpa.hibernate;

import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import javax.persistence.EntityManagerFactory;
import javax.persistence.spi.PersistenceProvider;
import javax.persistence.spi.PersistenceUnitInfo;
import javax.persistence.spi.ProviderUtil;

import org.amdatu.jpa.adapter.PersistenceProviderAdapter;
import org.hibernate.cfg.AvailableSettings;
import org.hibernate.engine.transaction.jta.platform.spi.JtaPlatform;
import org.hibernate.jpa.HibernatePersistenceProvider;
import org.osgi.framework.Bundle;
import org.osgi.framework.FrameworkUtil;

public class DelegatingHibernatePersistenceProvider implements PersistenceProvider, PersistenceProviderAdapter {

	private volatile JtaPlatform m_jtaPlatform;
	
	private final HibernatePersistenceProvider m_hibernatePersistenceProvider = new HibernatePersistenceProvider();
	
	@SuppressWarnings("unchecked")
	@Override
	public EntityManagerFactory createContainerEntityManagerFactory(PersistenceUnitInfo info, @SuppressWarnings("rawtypes") Map properties) {
		
		ClassLoader classLoader =  info.getClassLoader(); 
		
		ClassLoader tccl = Thread.currentThread().getContextClassLoader();
		try { 
			/*
			 *	Add JTA Platform to properties if not already present 
			 */
			@SuppressWarnings("rawtypes")
			Map p = new HashMap(); 
			if (properties != null){
				p.putAll(properties);
			}
			if (!properties.containsKey(AvailableSettings.JTA_PLATFORM)){
				p.put(AvailableSettings.JTA_PLATFORM, m_jtaPlatform);
			}
			 
			URL resource = classLoader.getResource("/");
			String host = resource.getHost();
			
			Bundle bundle = FrameworkUtil.getBundle(getClass()).getBundleContext().getBundle(Double.valueOf(host).intValue());
			
			p.put(org.hibernate.jpa.AvailableSettings.SCANNER, new ScannerImpl(bundle));
			
			Thread.currentThread().setContextClassLoader(classLoader);
			EntityManagerFactory emf = m_hibernatePersistenceProvider.createContainerEntityManagerFactory(info, p);
			return  new EntityManagerFactoryTcclWrapper(emf, classLoader);
		}finally{
			Thread.currentThread().setContextClassLoader(tccl);
		}
		
	}

	@Override
	public EntityManagerFactory createEntityManagerFactory(String info, @SuppressWarnings("rawtypes") Map properties) {
		throw new IllegalStateException("Not supported");
	}

	@Override
	public void generateSchema(PersistenceUnitInfo info, @SuppressWarnings("rawtypes") Map map) {
		m_hibernatePersistenceProvider.generateSchema(info, map);
	}

	@Override
	public boolean generateSchema(String persistenceUnitName, @SuppressWarnings("rawtypes") Map map) {
		return m_hibernatePersistenceProvider.generateSchema(persistenceUnitName, map);
	}

	@Override
	public ProviderUtil getProviderUtil() {
		return m_hibernatePersistenceProvider.getProviderUtil();
	}

	@Override
	public String getDynamicImports() {
		return "*";
	}	
	
}

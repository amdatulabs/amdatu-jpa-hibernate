/**
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.jpa.hibernate.itest.testbundle.impl;

import java.util.List;

import javax.persistence.EntityManager;

import org.amdatu.jpa.hibernate.itest.testbundle.TestEntity;
import org.amdatu.jpa.hibernate.itest.testbundle.TestService;
import org.amdatu.jta.Transactional;

@Transactional
public class TestServiceImpl implements TestService{

	private volatile EntityManager em;

	public TestServiceImpl() {
		super();
	}

	@Override
	public long save(TestEntity testEntity) {
		em.persist(testEntity);
		return testEntity.getId();
	}

	@Override
	public List<TestEntity> list() {
		return em.createQuery("select t from TestEntity t", TestEntity.class).getResultList();
	}

	@Override
	public void clearDb() {
		em.createQuery("delete from TestEntity t").executeUpdate();
	}

	@Override
	public void update(TestEntity testEntity) {
		em.merge(testEntity);
	}

	@Override
	public TestEntity get(long id) {
		return em.find(TestEntity.class, id);
	}

}